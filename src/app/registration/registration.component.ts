import { Component, OnInit,Inject } from '@angular/core';
import { CouchService } from '../couch.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

// export interface User{
//   first_name:string;
//   last_name:string;
//   email:string;
//   phone:string;
//   password:string;
// }

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  public user: any;
  constructor(private couchService:CouchService,
    private router:Router,
    @Inject(LOCAL_STORAGE) private storage:WebStorageService) {
      this.user = {};
  }


registerMe():void{
this.user.type='user';
this.user.role='reader';

  this.couchService.saveRegistration(this.user).subscribe((data:any[])=>{
     if(data['ok']){
      Swal.fire('Woow.', 'Your registration is completed successfully.', 'success');
      this.router.navigate(['/login']);
     }
     else{
      Swal.fire('Oops...', 'Something went wrong!', 'success');
     }
    })
}

  ngOnInit() {
    let userInfo=this.storage.get('userInfo');
   if(this.storage.get('loggedIn')){
    this.router.navigate(['/login']);  

   }
  }

  

}
