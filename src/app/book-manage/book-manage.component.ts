import { Component, OnInit } from '@angular/core';
import { CouchService } from '../couch.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-book-manage',
  templateUrl: './book-manage.component.html',
  styleUrls: ['./book-manage.component.css']
})
export class BookManageComponent implements OnInit {
  public book: any;
  public showSpinner: any;
  public categories: any;

  public rowId: string=undefined;
  
  constructor(private couchService:CouchService,
    private activeRoute:ActivatedRoute,
    private router:Router) { 
  this.book={};

this.activeRoute.params.subscribe(params => {
  this.rowId = params['bookId'];
});

if(this.rowId){
   this.couchService.getBook(this.rowId)
   .subscribe((data:any[])=>{
    this.book=data;
   },errors=>{
  
   });
}

this.categories=[
  'Action and Adventure',
'Anthology',
'Classic',
'Comic and Graphic Novel',
'Crime and Detective',
'Drama',
'Fable',
'Fairy Tale',
'Fan-Fiction',
'Fantasy',
'Historical Fiction',
'Horror',
'Humor',
'Legend',
'Magical Realism',
'Mystery',
'Mythology',
'Realistic Fiction',
'Romance',
'Satire',
'Science Fiction (Sci-Fi)',
'Short Story',
'Suspense/Thriller',
];
this.showSpinner=0;


  }
  savenewBook():void{
    this.showSpinner=1;
    if(this.rowId){
      this.book.id=this.book.category+'-'+this.book.isbn;
      this.couchService.updateBook(this.book).subscribe((data:any[])=>{
        this.book._rev=data['rev'];
        this.showSpinner=0;
        if(data['ok']){
          Swal.fire('Success.', 'Book details updated.', 'success');
         }
         else{
          Swal.fire('Oops...', 'Something went wrong!', 'success');
         }
    })

    }else{
      this.book.type='book';
      this.book.id=this.book.category+'-'+this.book.isbn;
      console.log(this.book);
      this.couchService.saveNewBook(this.book).subscribe((data:any[])=>{
        this.showSpinner=0;
        if(data['ok']){
          this.router.navigate(['/book-manage', data['id']]);
          Swal.fire('Success.', 'New book added with id '+this.book.id, 'success');
         }
         else{
          Swal.fire('Oops...', 'Something went wrong!', 'success');
         }
    })
    }
   
  }
  ngOnInit() {
    
  }

}
