import { Component, OnInit } from '@angular/core';
import { CouchService } from '../couch.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material';
import { AssignBookModalComponent } from '../assign-book-modal/assign-book-modal.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';


export interface Book {
  row_id:string;
  id: string;
  name: string;
  category: string;
  author: string;
  published_on:string;
  isbn:string;
}

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  public books : Book[]=[];
  public fields: any;
  public search : any=[];
  private modalRef: BsModalRef;
  
  displayedColumns: string[] = ['id', 'name','isbn', 'category', 'author','published_on','action'];

  constructor(private couchService:CouchService,
    private router:Router,
    public dialog:MatDialog,
    private modalService: BsModalService
    
    ) {
      this.fields=[
        {
          'key':'id',
          'field':'ID'
        },
        {
          'key':'name',
          'field':'Name'
        },
        {
          'key':'isbn',
          'field':'ISBN'
        },
        {
          'key':'category',
          'field':'Category'
        },
        {
          'key':'author',
          'field':'Author'
        }
      ];
   }

  ngOnInit() {
    this.loadBooks();
  }

  loadBooks():void{
    this.couchService.getBooks().subscribe((data:any[])=>{
      if(data['total_rows']){
   this.books=data['rows'].map((book:any)=>{
          book.value.row_id=  book.id;
       return book.value;
        });

      }else{
        this.books=[];
      }
    },errors=>{

    })
  }

  editBook(rowId:string):void{
   this.router.navigate(['/book-manage',rowId]);
  }  
  deleteBook(book:any):void{  
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this book back!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

 this.couchService.deleteBook(book).
    subscribe((data:any[])=>{
    if(data['ok']){
      this.loadBooks();
      Swal.fire(
        'Deleted!',
        'Book has been deleted.',
        'error'
      );
    }
      }
    ,errors=>{

   })
     
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'book is safe :)',
          'error'
        );
      }
    })
   
}
generateSearchJson(search:any):any{
 let template= {
   "selector":{},
    "fields": [
       "_id",
       "id",
       "type",
       "name",
       "isbn",
       "category",
       "author",
       "published_on",
       "userId"
    ],
    "execution_stats": true
 }

 template['selector'][search['field']]={
  '$regex':search['key']
 };

 return template;
}

searchTimeout = null;

onSearchKeywords():void {
    if (this.searchTimeout)
        clearTimeout(this.searchTimeout);
    this.searchTimeout = setTimeout(() => {
      if(this.search.field!=undefined&&this.search.key!=undefined&&this.search.field!=""){
        this.couchService._findDcouments(this.generateSearchJson(this.search)).
        subscribe((data:any)=>{
          this.books=data.docs.map((book:any)=>{
            book.row_id=book._id;
         return book;
        });
     
          }
        ,errors=>{
    
       })
      }

    }, 500);
}

assignBook(book:any):void
 {
  if(book.userId==undefined){
  this.router.navigate(['/assign-book',book.row_id]);
  return ;
  }
  Swal.fire(
    '',
    'Already assigned to a user.',
    'error'
  );


    // this.modalRef = this.modalService.show(AssignBookModalComponent, {
      
		// 	initialState: {
		// 	}
    // });
    // this.modalRef.content.onClose.subscribe(result => {

    // })
}

}
