import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { CouchService } from '../couch.service';
import { MatSnackBar } from '@angular/material';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

export interface Credentials{
  username:string;
  password:string;
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})


export class AuthComponent implements OnInit {

 public credential:Credentials ={
  username:'',
  password:''
 };

  constructor(private router: Router,
    private couchService: CouchService,
    private _snackBar:MatSnackBar,
    @Inject(LOCAL_STORAGE) private storage:WebStorageService) { }


  ngOnInit() {
    let userInfo=this.storage.get('userInfo');
   if(this.storage.get('loggedIn')){
    if(userInfo['role']=="reader")
    {
      this.router.navigate(['/reader-books']);          
    }else{
      this.router.navigate(['/book']);  
    }
   }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  login():void{

  let template= {
    "selector":{},
     "fields": [
        "_id",
        "first_name",
        "last_name",
        "role"
     ],
     "execution_stats": true
  }
 
  template['selector']={
    'type':"user",
    'username':this.credential.username,
    'password':this.credential.password
  };
    this.couchService._findDcouments(template).
    subscribe((data:any)=>{
      if(data.docs.length){//success
        this.storage.set('loggedIn',1);
        this.storage.set('userInfo',data.docs[0]);
        if(data.docs[0]['role']=="reader")
        {
          this.router.navigate(['/reader-books']);          
        }else{
          this.router.navigate(['/book']);  
        }

      }else{
        this.openSnackBar("No user found.","");
      }
      }
    ,errors=>{
      this.openSnackBar("Something went wrong.","");
   })
  }

}
