import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-assign-book-modal',
  templateUrl: './assign-book-modal.component.html',
  styleUrls: ['./assign-book-modal.component.css']
})
export class AssignBookModalComponent implements OnInit {

  public onClose: Subject<boolean>;

  constructor(){}

  ngOnInit() {
    this.onClose = new Subject();
  }

}
