import { TestBed } from '@angular/core/testing';

import { CouchService } from './couch.service';

describe('CouchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CouchService = TestBed.get(CouchService);
    expect(service).toBeTruthy();
  });
});
