import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { AuthComponent } from './auth/auth.component';
import { BookComponent } from './book/book.component';
import { BookManageComponent } from './book-manage/book-manage.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { ManageUserComponent } from './manage-user/manage-user.component';
import { ReaderBooksComponent } from './reader-books/reader-books.component';
import { CommentComponent } from './comment/comment.component';
import { AssignBookComponent } from './assign-book/assign-book.component';


const routes: Routes = [
  {path:'registration' ,
  component:RegistrationComponent
  },
  {
    path:'login',
    component:AuthComponent
  },
  {
    path:'',
    component:HomeComponent,
    children:[
      {
				path: '',
				redirectTo: '/book',
				pathMatch: 'full'
			},
      {
        path:'book',
        component:BookComponent
      },
    
      {
        path:'book-manage',
        component:BookManageComponent
      }
    ,
    {
      path:'book-manage/:bookId',
      component:BookManageComponent
    },
    {
      path:'users',
      component:UsersComponent
    },
    {
      path:'manage-user',
      component:ManageUserComponent
    },
    {
      path:'reader-books',
      component:ReaderBooksComponent
    },
    {
      path:'comment/:bookId',
      component:CommentComponent
    },
    {
      path:'assign-book/:bookId',
      component:AssignBookComponent
    },
  {
    path:'manage-user/:userId',
    component:ManageUserComponent
  }
    ]
  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
