import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { RegistrationComponent } from './registration/registration.component';
import { AuthComponent } from './auth/auth.component';
import { BookComponent } from './book/book.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatButtonModule,
  MatProgressSpinnerModule, 
  MatTableModule,
  MatInputModule,
  MatFormFieldModule,
  MatMenuModule,
  MatSelectModule,
  MatDialogModule,
  MatGridListModule,
  MatSnackBarModule} from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BookManageComponent } from './book-manage/book-manage.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { HomeComponent } from './home/home.component';
import { NgxMaskModule ,IConfig} from 'ngx-mask';
import { UsersComponent } from './users/users.component';
import { ManageUserComponent } from './manage-user/manage-user.component';
import { AssignBookModalComponent } from './assign-book-modal/assign-book-modal.component';
import { ReaderBooksComponent } from './reader-books/reader-books.component';
import { CommentComponent } from './comment/comment.component';
// import { FlexLayoutModule } from '@angular/flex-layout';
import { StorageServiceModule} from 'angular-webstorage-service';
import { AssignBookComponent } from './assign-book/assign-book.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    AuthComponent,
    BookComponent,
    BookManageComponent,
    ConfirmDialogComponent,
    HomeComponent,
    UsersComponent,
    ManageUserComponent,
    AssignBookModalComponent,
    ReaderBooksComponent,
    CommentComponent,
    AssignBookComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatSelectModule,
    MatDialogModule,
    MatGridListModule,
    NgxMaskModule.forRoot(),
    MatSnackBarModule,
    ModalModule.forRoot(),
    //  FlexLayoutModule,
    StorageServiceModule
    

  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[AssignBookModalComponent]
})
export class AppModule { }


