import { Component, OnInit } from '@angular/core';
import { CouchService } from '../couch.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.css']
})
export class ManageUserComponent implements OnInit {

  public user: any;
  public rowId: string=undefined;

  constructor(private couchService:CouchService,
    private router:Router,
    private activeRoute:ActivatedRoute) {
      this.user = {};

      this.activeRoute.params.subscribe(params => {
        this.rowId = params['userId'];
      });

      if(this.rowId){
        this.couchService.getUser(this.rowId)
        .subscribe((data:any[])=>{
         this.user=data;
        },errors=>{
       
        });
     }
      
  }
  pad(num:number, size:number): string {
    let s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
generateUserId(currentDocNo:number,username:string):string{
let userId="";
userId=username+'-'+this.pad((currentDocNo+1),5);
return userId;
}

  ngOnInit() {
   
  }

  manageUser():void{
this.user.type='user';
if(this.rowId){//update
  this.couchService.updateBook(this.user).subscribe((data:any[])=>{
    this.user._rev=data['rev'];
    if(data['ok']){
      Swal.fire('Success.', 'User details updated.', 'success');
     }
     else{
      Swal.fire('Oops...', 'Something went wrong!', 'success');
     }
})
}else{
  this.couchService.getUserStat().subscribe((data:any[])=>{
    let userIdMaxno=data['rows'][0]['value']['max'];
    console.log('userIdMaxno'+userIdMaxno);
      this.user.id= this.generateUserId(userIdMaxno,this.user.username);
      this.user.related_no=userIdMaxno==0?1:userIdMaxno+1;
      this.user.status='active';
      console.log(this.user);
      this.couchService.saveNewUser(this.user).subscribe((data:any[])=>{
    if(data['ok']){
      //console.log(data);
      this.router.navigate(['/manage-user', data['id']]);
      Swal.fire('Success.', 'New user added with id '+this.user.id, 'success');
     }
     else{
      Swal.fire('Oops...', 'Something went wrong!', 'success');
     }
  })  
  })
 
}



  }

}
