import { Component, OnInit } from '@angular/core';
import { CouchService } from '../couch.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-assign-book',
  templateUrl: './assign-book.component.html',
  styleUrls: ['./assign-book.component.css']
})
export class AssignBookComponent implements OnInit {
public book:any={};
public users:any=[];
public bookId:string="";
public userId:string="";

  constructor(private couchService:CouchService,
    private router:Router,
    private activeRoute:ActivatedRoute,) {

      this.activeRoute.params.subscribe(params => {
        this.bookId = params['bookId'];
      });
     }

  ngOnInit() {
    this.couchService.getBook(this.bookId)
    .subscribe((data:any[])=>{
     this.book=data;
    },errors=>{
   
    });

let selector={
  "selector": {
     "status": "active",
     "type": "user",
     "role": "reader"
  },
  "fields": [
     "_id",
     "first_name",
     "last_name"
  ],
  "execution_stats": true
};

this.couchService._findDcouments(selector).
        subscribe((data:any)=>{
          this.users=data.docs;
          }
        ,errors=>{
    
       })

  }


  assignBook():void{
        
      this.book.userId=this.userId;
      this.book.row_id=this.bookId;
      this.couchService.assignBook(this.book).subscribe((data:any[])=>{
      
        if(data['ok']){
          Swal.fire('Success.', 'User assigned.', 'success');
          this.router.navigate(['/book']);
         }
         else{
          Swal.fire('Oops...', 'Something went wrong!', 'success');
         }
    })

  }

}
