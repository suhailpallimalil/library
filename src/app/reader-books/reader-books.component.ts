import { Component, OnInit,Inject } from '@angular/core';
import { CouchService } from '../couch.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';


export interface Book {
  row_id:string;
  id: string;
  name: string;
  category: string;
  author: string;
  published_on:string;
  isbn:string;
  _rev:string;
  type:string;
}

@Component({
  selector: 'app-reader-books',
  templateUrl: './reader-books.component.html',
  styleUrls: ['./reader-books.component.css']
})
export class ReaderBooksComponent implements OnInit {
  public books : Book[]=[];
  public fields: any;
  public search : any=[];
  public bookListMode:string='all_books';
  displayedColumns: string[] = ['id', 'name','isbn', 'category', 'author','published_on','action'];

  constructor(private couchService:CouchService,
    private router:Router,
    public dialog:MatDialog,
    @Inject(LOCAL_STORAGE) private storage:WebStorageService
    ) {
      this.fields=[
        {
          'key':'id',
          'field':'ID'
        },
        {
          'key':'name',
          'field':'Name'
        },
        {
          'key':'isbn',
          'field':'ISBN'
        },
        {
          'key':'category',
          'field':'Category'
        },
        {
          'key':'author',
          'field':'Author'
        }
      ];
   }

  ngOnInit() {
    this.loadBooks();
  }

  loadBooks():void{
    this.couchService.getBooks().subscribe((data:any[])=>{
      if(data['total_rows']){
   this.books=data['rows'].map((book:any)=>{
          book.value.row_id=  book.id;
       return book.value;
        });
        // console.log(this.books);
      }else{
        this.books=[];
      }
    },errors=>{

    })
  }

  makeComment(book:any):void{  
this.router.navigate(['/comment',book['row_id']]);
  }
  listMyBooks():void{
if(this.bookListMode=="all_books"){
  this.bookListMode='my_books';
  let userId =this.getLoggedInUser()['_id'];
  let template= {
    "selector":{},
     "fields": [
        "_id",
        "id",
        "name",
        "isbn",
        "category",
        "author",
        "published_on",
        "_rev",
        "type"
     ],
     "execution_stats": true
  }
 
  template['selector']={
    'type':"book",
    'userId':userId
  };
    this.couchService._findDcouments(template).
    subscribe((data:any)=>{
      console.log(data);
      this.books=data.docs.map((book:any)=>{
        book.row_id=book._id;
     return book;
    });
      }
    ,errors=>{
   })
}else{
  this.bookListMode='all_books';
  this.loadBooks();
}

}

getLoggedInUser():object{
 return  this.storage.get('userInfo');
}

generateSearchJson(search:any):any{
 let template= {
   "selector":{},
    "fields": [
       "_id",
       "id",
       "name",
       "isbn",
       "category",
       "author",
       "published_on"
    ],
    "execution_stats": true
 }

 template['selector'][search['field']]={
  '$regex':search['key']
 };

 return template;
}

searchTimeout = null;

onSearchKeywords():void {
    if (this.searchTimeout)
        clearTimeout(this.searchTimeout);
    this.searchTimeout = setTimeout(() => {
      if(this.search.field!=undefined&&this.search.key!=undefined&&this.search.field!=""){
        this.couchService._findDcouments(this.generateSearchJson(this.search)).
        subscribe((data:any)=>{
          this.books=data.docs.map((book:any)=>{
            book.row_id=book._id;
         return book;
        });
     
          }
        ,errors=>{
    
       })
      }

    }, 500);
}

returnBook(book:any):void{
  Swal.fire({
    title: 'Are you sure?',
    text: 'You want to return this book.',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, return it!',
    cancelButtonText: 'No, keep it'
  }).then((result) => {
    if (result.value) {

this.couchService.updateBook(book).
  subscribe((data:any[])=>{
  if(data['ok']){
    this.loadBooks();
    Swal.fire(
      'Success!',
      'Book returned',
      'success'
    );
  }
    }
  ,errors=>{

 })
   
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      Swal.fire(
        'Cancelled',
        'book is safe :)',
        'error'
      );
    }
  })
}

}
