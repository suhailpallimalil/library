import { Component, OnInit ,Inject} from '@angular/core';
import { CouchService } from '../couch.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MatSnackBar } from '@angular/material';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

public bookId:string=undefined;
public comments:any=[];
public description:string="";
  constructor(private couchService:CouchService,
    private activeRoute:ActivatedRoute,
    private router:Router,
    private _snackBar:MatSnackBar,
    @Inject(LOCAL_STORAGE) private storage:WebStorageService
    ) { 
    this.activeRoute.params.subscribe(params => {
      this.bookId = params['bookId'];
    });
   }

  ngOnInit() {
this.loadComments();
  }

  loadComments():any{

    let template= {
      "selector":{},
       "fields": [
          "_id",
          "_rev",
          "description",
          "user",
          "userId"
       ],
       "execution_stats": true
    }
   
    template['selector']={
      'type':"comment",
      'bookId':this.bookId
    };
      this.couchService._findDcouments(template).
      subscribe((data:any)=>{
        this.comments=data.docs;
        console.log(this.comments)
        }
      ,errors=>{
     })
  }

  deleteComment(comment:any):void{
let loogedInUser=this.getLoggedInUser();
if(comment.userId!=loogedInUser['_id']){
  this.openSnackBar("You can only delete thee comment that you have created.","");
  return;
}
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this comment back!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

 this.couchService.deleteComment(comment).
    subscribe((data:any[])=>{
    console.log(data);
    if(data['ok']){
      this.loadComments();
      Swal.fire(
        'Deleted!',
        'Comment has been deleted.',
        'success'
      );
    }
      }
    ,errors=>{

   })
     
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Comment is safe :)',
          'error'
        );
      }
    })
  }
  getLoggedInUser():object{
    return  this.storage.get('userInfo');
   }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  saveComment():void{
 if(this.description==""){
  this.openSnackBar("Please enter any comment.","");
  return;
 }
 let loogedInUser=this.getLoggedInUser();

 let comment={
   'description':this.description,
   'type':"comment",
   'bookId':this.bookId,
   'user':loogedInUser['first_name']+' '+loogedInUser['last_name'],
   'userId':loogedInUser['_id']
 };
 this.couchService.saveComment(comment)
 .subscribe((data:any[])=>{
  console.log(data);
  if(data['ok']){
    this.description='';
    this.loadComments();
    Swal.fire(
      'Success!',
      'Comment has been created.',
      'success'
    );
  }
    }
  ,errors=>{

 })
 console.log(comment);


  }

}
