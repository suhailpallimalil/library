import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CouchService {

  // private SERVER_URL = "https://5bbfb93b-dd92-44c2-b69e-62424feaf129-bluemix.cloudantnosqldb.appdomain.cloud/";
 private SERVER_URL = "http://127.0.0.1:5984/";

 

  private DATABASE="library";
  constructor(private httpClient:HttpClient) { }

  public getBooks(){  
		return this.httpClient.get(this.SERVER_URL+this.DATABASE+'/_design/books/_view/books');  
  }  
  
  public saveRegistration(data:any){  
		return this.httpClient.post(this.SERVER_URL+this.DATABASE,data);  
  }
  
  public saveNewBook(book:any){  
		return this.httpClient.post(this.SERVER_URL+this.DATABASE,book);  
  }
  public updateBook(book:any){  
		return this.httpClient.put(this.SERVER_URL+this.DATABASE+'/'+book._id,book);  
  }
  public getBook(rowId:string){  
		return this.httpClient.get(this.SERVER_URL+this.DATABASE+'/'+rowId);  
  }
  public deleteBook(book:any){  
		return this.httpClient.delete(this.SERVER_URL+this.DATABASE+'/'+book.row_id+'?rev='+book._rev);  
  }
  
  public saveNewUser(user:any){
		return this.httpClient.post(this.SERVER_URL+this.DATABASE,user);  
  }
  public updateUser(user:any){  
		return this.httpClient.put(this.SERVER_URL+this.DATABASE+'/'+user._id,user);  
  }
  public getUser(rowId:string){  
		return this.httpClient.get(this.SERVER_URL+this.DATABASE+'/'+rowId);  
  }

  public getUsers(){  
		return this.httpClient.get(this.SERVER_URL+this.DATABASE+'/_design/users/_view/users');  
  }  
  public deleteUser(user:any){  
		return this.httpClient.delete(this.SERVER_URL+this.DATABASE+'/'+user.row_id+'?rev='+user._rev);  
  }
  public getUserStat(){  
		return this.httpClient.get(this.SERVER_URL+this.DATABASE+'/_design/users/_view/user-stat');  
  }  
  public _findDcouments(data:any){  
		return this.httpClient.post(this.SERVER_URL+this.DATABASE+'/_find',data);  
  }  

  public assignBook(book:any){  
		return this.httpClient.put(this.SERVER_URL+this.DATABASE+'/'+book.row_id,book);  
  }

  public deleteComment(comment:any){  
		return this.httpClient.delete(this.SERVER_URL+this.DATABASE+'/'+comment._id+'?rev='+comment._rev);  
  }
  public saveComment(comment:any){
		return this.httpClient.post(this.SERVER_URL+this.DATABASE,comment);  
  }
}
