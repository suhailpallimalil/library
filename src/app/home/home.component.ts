import { Component, OnInit,Inject } from '@angular/core';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
public menus:any={};
public userMenus:any=[];
public userRole:string='';
  constructor( @Inject(LOCAL_STORAGE) private storage:WebStorageService,
  private router:Router) {
     this.menus = {"admin":[{"menuName":"Users","menuLink":"users"},
                            {"menuName":"Books","menuLink":"book"}],
                  "librarian":[{"menuName":"Books","menuLink":"book"}],
                  "reader":[{"menuName":"Books","menuLink":"reader-books"}]
};
   }
  ngOnInit() { 
    let userInfo=this.storage.get('userInfo');
    this.userRole=userInfo['role'];
   if(this.storage.get('loggedIn')){
     this.userMenus=this.menus[userInfo['role']];
   }else{
    this.router.navigate(['/login']);
   }
  }

  menuClick(menu:any):void{
this.router.navigate([menu['menuLink']]);
  }
  logout():void{
    this.storage.remove('loggedIn');
    this.storage.remove('userInfo');
    this.router.navigate(['/login']);

  }

}
