import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CouchService } from '../couch.service';
import Swal from 'sweetalert2';


export interface User {
  row_id:string;
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  phone_number:string;
  username:string;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public users : User[]=[];

  displayedColumns: string[] = ['id', 'first_name','last_name','role','email','phone_number','username','status','action'];

  constructor(private couchService:CouchService,
    private router:Router) {
 
   }

  ngOnInit() {
    this.loadUsers();
  }
  loadUsers():void{
    this.couchService.getUsers().subscribe((data:any[])=>{
      if(data['total_rows']){
   this.users=data['rows'].map((user:any)=>{
    user.value.row_id=  user.id;
       return user.value;
        });
      }else{
        this.users=[];
      }
    },errors=>{

    })
  }

  editUser(rowId:string):void{
    this.router.navigate(['/manage-user',rowId]);
   }  

   deleteUser(user:any):void{  
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this user back!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {

 this.couchService.deleteUser(user).
    subscribe((data:any[])=>{
    console.log(data);
    if(data['ok']){
      this.loadUsers();
      Swal.fire(
        'Deleted!',
        'User has been deleted.',
        'success'
      );
    }
      }
    ,errors=>{

   })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'User is safe :)',
          'error'
        );
      }
    })
}

changeStatus(user:any):void{
  Swal.fire({
    title: 'Are you sure?',
    text: 'User become '+(user.status=='active'?'inactive':'active'),
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, change status !',
    cancelButtonText: 'No, keep it'
  }).then((result) => {
    if (result.value) {
     // let getUser={};
      this.couchService.getUser(user.row_id)
        .subscribe((data:any[])=>{
        //  getUser=data;
        data['status']= (user.status=='active'?'inactive':'active');
       this.couchService.updateBook(data).subscribe((data:any[])=>{
         if(data['ok']){
           this.loadUsers();
           Swal.fire('Success.', 'User status has been updated', 'success');
          }
          else{
           Swal.fire('Oops...', 'Something went wrong!', 'success');
          }
     }
   ,errors=>{
  })
        },errors=>{
        });

       
    } else if (result.dismiss === Swal.DismissReason.cancel) {
      Swal.fire(
        'Cancelled',
        'User is safe :)',
        'error'
      );
    }
  })
}
}
